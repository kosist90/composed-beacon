# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## [0.9.8] - 2019-02-27
### Changed
- updated dependencies

## [0.9.7] - 2019-02-27
### Added
- Added test for beacon discovery in <3 seconds
- Updated composed-ci.cfg for new CI package version
- reduced pre launch init timeout when checking for duplicate names

## [0.9.0] - 2019-02-21
### Added
Everything - initial release